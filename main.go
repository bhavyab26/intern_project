package main

import (
	"fmt"
	_ "in_project/routers"
	_"in_project/models"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego"
	_ "github.com/go-sql-driver/mysql"
	"github.com/astaxie/beego/plugins/auth"
)

func init() {
    orm.RegisterDriver("mysql", orm.DRMySQL)

    orm.RegisterDataBase("default", "mysql", "root:root123@/sales?charset=utf8")

    name := "default"

	force := false	// set to false after executing once

	verbose := false // set to false after executing once

	err := orm.RunSyncdb(name, force, verbose)
	if err != nil {
    	fmt.Println(err)
    }

    

}

func main() {
   
	beego.BConfig.WebConfig.Session.SessionOn = true
	beego.InsertFilter("/appadmin/*", beego.BeforeRouter, auth.Basic("youradminname", "YourAdminPassword"))
	beego.Run()
}


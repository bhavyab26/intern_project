package controllers

import (
	"in_project/models"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
	//"github.com/twinj/uuid"
	"html/template"
	"strconv"
	"strings"
	//"time"
)

type AdminController struct {
	beego.Controller
}

func (this *AdminController) activeAdminContent(view string) {

	this.TplName = view + ".tpl"
	this.Data["domainname"] = "localhost:8080"
}

type compareform struct {
	Comparefield string `form:"comparefield"`
	Compareop    string `form:"compareop"`
	Compareval   string `form:"compareval" valid:"Required"`
}

func (this *AdminController) setCompare(query string) (orm.QuerySeter, bool) {
	o := orm.NewOrm()
	qs := o.QueryTable("sales_officer")
	if this.Ctx.Input.Method() == "POST" {
		f := compareform{}
		if err := this.ParseForm(&f); err != nil {
			fmt.Println("cannot parse form")
			return qs, false
		}
		valid := validation.Validation{}
		if b, _ := valid.Valid(&f); !b {
			this.Data["Errors"] = valid.ErrorsMap
			return qs, false
		}
		if len(f.Compareop) >= 5 && f.Compareop[:5] == "__not" {
			qs = qs.Exclude(f.Comparefield+f.Compareop[5:], f.Compareval)
		} else {
			qs = qs.Filter(f.Comparefield+f.Compareop, f.Compareval)
		}
		this.Data["query"] = f.Comparefield + f.Compareop + "," + f.Compareval
	} else {
		str := strings.Split(query, ",")
		i := strings.Index(str[0], "__")
		if len(str[0][i:]) >= 5 && str[0][i:i+5] == "__not" {
			qs = qs.Exclude(str[0][:i]+str[0][i+5:], str[1])
		} else {
			qs = qs.Filter(str[0], str[1])
		}
		this.Data["query"] = query
	}
	return qs, true
}

func max(a, b int64) int64 {
	if a < b {
		return b
	}
	return a
}

func (this *AdminController) Index() {
	this.activeAdminContent("appadmin/index")

	defer func(this *AdminController) {
		if r := recover(); r != nil {
			fmt.Println("Recovered in Index", r)
			this.Redirect("/home", 302)
		}
	}(this)

	const pagesize = 10
	parms := this.Ctx.Input.Param(":parms")
	this.Data["parms"] = parms
	str := strings.Split(parms, "!")
	fmt.Println("parms is", str)
	order := str[0]
	off, _ := strconv.Atoi(str[1])
	offset := int64(off)
	if offset < 0 {
		offset = 0
	}
	query := str[2]

	var users []*models.SalesOfficer
	rows := ""

	qs, ok := this.setCompare(query)
	if !ok {
		fmt.Println("cannot set QuerySeter")
		o := orm.NewOrm()
		qs := o.QueryTable("sales_officer")
		qs = qs.Filter("id__gte", 0)
		this.Data["query"] = "id__gte,0"
	}

	count, _ := qs.Count()
	this.Data["count"] = count
	if offset >= count {
		offset = 0
	}
	num, err := qs.Limit(pagesize, offset).OrderBy(order).All(&users)
	if err != nil {
		fmt.Println("Query table failed:", err)
	}
	domainname := this.Data["domainname"]
	for x := range users {
		rows += fmt.Sprintf("<tr><td><a href='http://%s/appadmin/update/%s!%s'>%d</a></td>"+
			"<td>%s</td><td>%d</td><td>%d</td><td>%d</td></tr>>", domainname, users[x].Email, parms,
			users[x].Id, users[x].Name, users[x].ParentId, users[x].Lft, users[x].Rgt)
	}
	this.Data["Rows"] = template.HTML(rows)

	this.Data["order"] = order
	this.Data["offset"] = offset
	this.Data["end"] = max(0, count-pagesize)
	if num+offset < count {
		this.Data["next"] = num + offset
	}
	if offset-pagesize >= 0 {
		this.Data["prev"] = offset - pagesize
		this.Data["showprev"] = true
	} else if offset > 0 && offset < pagesize {
		this.Data["prev"] = 0
		this.Data["showprev"] = true
	}

	if count > pagesize {
		this.Data["ShowNav"] = true
}
}



// func (s *SalesOfficer) Valid(v *validation.Validation) {
//     if ( s.ParentId == null ) {
//     	s.lft = 1;
//     	s.Rgt = 2;
//     }
//     else {
    	
//     }
// }

func (this *AdminController) Add() {
	this.activeAdminContent("appadmin/add")

	parms := this.Ctx.Input.Param(":parms")
	this.Data["parms"] = parms

	if this.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		u := models.SalesOfficer{}
		if err := this.ParseForm(&u); err != nil {
			fmt.Println("cannot parse form")
			return
		}
		this.Data["User"] = u
		valid := validation.Validation{}
		if b, _ := valid.Valid(&u); !b {
			this.Data["Errors"] = valid.ErrorsMap
			return
		}

		//******** Save user info to database
		o := orm.NewOrm()
		o.Using("default")


		// qs := o.QueryTable("sales_officer")
		// qs = qs.Filter("id__gte", 0)
		// qs.Filter("profile__age", 18)
		// var count int
 	// 	o.Raw("select count(*) as Count from SalesOfficer").QueryRow(&count)

		// Nesting not implemented for adding new node

		// if ( u.ParentId == null ) {
		// 	u.Lft = 1
		// 	u.Rgt = 2
		// }
		// else {
		// 	qs := o.QueryTable("sales_officer")
		// 	qs = qs.Filter("parentid", u.ParentId)
		// 	myRight = qs.Rgt
			
		// }
 		

		_, err := o.Insert(&u)
		this.Data["User"] = u
		if err != nil {
			flash.Error(u.Email + " already registered")
			flash.Store(&this.Controller)
			return
		}

		flash.Notice("User added")
		flash.Store(&this.Controller)
	}
}

// type SalesOfficer struct {
// 	Id           int        `form:"id"`
// 	Name         string     `form:"name" valid:"Required"`
// 	ParentId     int        `form:"parentid"`
// 	Email        string     `form:"email" valid:"Email"`
// 	Lft          int        `form:"lft" valid:"Required"`
// 	Rgt          int        `form:"rgt" valid:"Required"`
	
// 	Delete       string     `form:"delete,checkbox"`
// }


func (this *AdminController) Report() {
	this.activeAdminContent("appadmin/report")

	if this.Ctx.Input.Method() == "POST" {
		flash := beego.NewFlash()
		s := models.Sale{}
		if err := this.ParseForm(&s); err != nil {
			fmt.Println("cannot parse form")
			return
		}
		this.Data["Sale"] = s
		valid := validation.Validation{}
		if b, _ := valid.Valid(&s); !b {
			this.Data["Errors"] = valid.ErrorsMap
			return
		}

		//******** Save report to database
		o := orm.NewOrm()
		o.Using("default")

		o.Insert(&s)
		this.Data["Sale"] = s
		// if err != nil {
		// 	flash.Error("Calls not be more than 1000")
		// 	flash.Store(&this.Controller)
		// 	return
		// }

		flash.Notice("Report Submitted")
		flash.Store(&this.Controller)

	}

}

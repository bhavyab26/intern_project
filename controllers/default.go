package controllers

import (
	"github.com/astaxie/beego"
	
)

type MainController struct {
	beego.Controller
}

func (this *MainController) activeContent( view string) {
	sess := this.GetSession("acme")
	if sess != nil {
		this.Data["InSession"] = 1 // for login bar in header.tpl
		m := sess.(map[string]interface{})
		this.Data["Name"] = m["name"]
	}
}

func (c *MainController) Get() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplName = "index.tpl"

}

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<h3 align="center">Add</h3>
{{.count}}
{{if .flash.error}}
<h3>{{.flash.error}}</h3>
<br>
{{end}}
{{if .flash.notice}}
<h3>{{.flash.notice}}</h3>
<br>
{{end}}

<div class="container">
	<form class="form-horizontal" method="POST">

		<div class="form-group">
    		<label class="control-label col-sm-2">ID:</label>
    		<div class="col-sm-8"> 
      			<input class="form-control" value="{{.User.Id}}" readonly>
    		</div>
  		</div>

		<div class="form-group">
    		<label class="control-label col-sm-2">Name: {{if .Errors.Name}}{{.Errors.Name}}{{end}}</label>
    		<div class="col-sm-8"> 
      			<input type="text" class="form-control" value="{{.User.Name}}" placeholder="Enter name">
    		</div>
  		</div>

  		<div class="form-group">
    		<label class="control-label col-sm-2">Email: {{if .Errors.Email}}{{.Errors.Email}}{{end}}</label>
    		<div class="col-sm-8">
      			<input type="email" class="form-control" value="{{.User.Email}}" placeholder="Enter email">
    		</div>
  		</div>

  		<div class="form-group">
    		<label class="control-label col-sm-2">Parent ID: {{if .Errors.ParentId}}{{.Errors.ParentId}}{{end}}</label>
    		<div class="col-sm-8"> 
      			<input type="number" class="form-control" value="{{.User.ParentId}}" placeholder="Enter parent id">
    		</div>
  		</div>

  		<div class="form-group">
    		<label class="control-label col-sm-2">Lft: {{if .Errors.Lft}}{{.Errors.Lft}}{{end}}</label>
    		<div class="col-sm-8"> 
      			<input type="number" class="form-control" value="{{.User.Lft}}" placeholder="Enter lft">
    		</div>
  		</div>
  		<div class="form-group">
    		<label class="control-label col-sm-2">Rgt: {{if .Errors.Rgt}}{{.Errors.Rgt}}{{end}}</label>
    		<div class="col-sm-8"> 
      			<input type="number" class="form-control" value="{{.User.Rgt}}" placeholder="Enter rgt">
    		</div>
  		</div>
  		<input type="submit" class="btn btn-primary" value="Add"/>
  		<!-- <div class="form-group"> 
    		<div class="col-sm-offset-2 col-sm-8">
      			
    		</div>
  		</div> -->
	</form>

	<div class="col-sm-offset-2 col-sm-8">
		<h4><a href="http://{{.domainname}}/appadmin/index/{{.parms}}">Return to Index</a></h4>
	</div>
</div>


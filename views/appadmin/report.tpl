<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<h3 align="center">Sales Report</h3>

<script src="http://cdnjs.cloudflare.com/ajax/libs/vue/2.4.4/vue.js"></script>
<script src="https://unpkg.com/vee-validate@2.0.0-rc.7"></script>
<script>
    Vue.use(VeeValidate); // good to go.
</script>



<form method="POST" class="form-horizontal" @submit.prevent="validateBeforeSubmit">
	<div class="container">

		<div class="form-group">
    		<label class="control-label col-sm-2">Key:</label>
    		<div class="col-sm-8"> 
      			<input type="number" class="form-control mb-2" placeholder="Rep" v-model="key" value="{{.Sale.Id}}" readonly="">
    		</div>
  		</div>

  		<div class="form-group">
    		<label class="control-label col-sm-2">Date:</label>
    		<div class="col-sm-8"> 
      			<input type="date" class="form-control mb-2" placeholder="Date" v-model="date">
    		</div>
  		</div>


  		<div class="form-group">
    		<label class="control-label col-sm-2">SKU:</label>
    		<div class="col-sm-8"> 
      			<input type="number" class="form-control mb-2" placeholder="SKU" v-for="(sku, index) in skus" v-model="skus[index]" :name="'sku' + index" value="{{.Sale.SKU}}">
    		</div>
  		</div>
				
		
		<div class="form-group">
    		<label class="control-label col-sm-2">Product Calls:</label>
    		<div class="col-sm-8"> 
      			<input type="number" class="form-control mb-2" placeholder="Product Calls" v-model="prodCalls" value="{{.Sale.ProdCalls}}" v-validate="'required|between:{1},{1000}'" :class="{'input': true, 'is-danger': errors.has('prodCalls') }">
            	<i v-show="errors.has('prodCalls')" class="fa fa-warning"></i>
            	<span v-show="errors.has('prodCalls')" class="help is-danger">{errors.first('prodCalls') }</span>
    		</div>
  		</div>

        
        <div class="form-group">
    		<label class="control-label col-sm-2">Total Calls:</label>
    		<div class="col-sm-8"> 
      			<input type="number" class="form-control mb-2" placeholder="Total Calls" v-model="totCalls" value="{{.Sale.TotCalls}}" v-validate="'required|between:{1},{1000}'" :class="{'input': true, 'is-danger': errors.has('totCalls') }">
            	<i v-show="errors.has('totCalls')" class="fa fa-warning"></i>
            	<span v-show="errors.has('totCalls')" class="help is-danger">{errors.first('totCalls') }</span>
    		</div>
  		</div>


				

		<div class="col-sm-8"> 
                Do you want to take a survey?
                <button class="btn btn-primary mb-2">
                    Yes
                </button>
                <button class="btn btn-primary mb-2">
                    No
                </button>
				<br>
				<button class="btn btn-primary mb-5" @click="addNewProduct">
					Add New Product
				</button>
				<input type="submit" class="btn btn-primary" value="Report"/>
				<br>
				<input type="button" class="btn btn-link mb-2" value="Return to Index" onclick="location.href = 'http://{{.domainname}}/appadmin/index/id!0!id__gte,0';">
		</div>
	</div>
</form>


<script>


    //Vue.use(VeeValidate);
	
	var app = new Vue({
		el: '.container',
		data: {

			key: '',
			date: '',
            skus: [null],
			prodCalls: '',
			totCalls: ''
				
		},
		methods: {
			addNewProduct: function() {
      			this.skus.push(this.skus[this.skus.length - 1])
    		}
    		validateBeforeSubmit: function() {
      			this.$validator.validateAll()
      
      			if (!this.errors.any()) {
        			alert('submit')
      			}
    		}
		}
	})
</script>



<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<br><br>
<div class="container">
	<h2>Sales Officers</h2>

	Total: {{.count}}
	
	<input type="button" class="btn btn-primary" value="Add a record" onclick="location.href = 'http://{{.domainname}}/appadmin/add/{{.parms}}';">
	<input type="button" class="btn btn-primary" value="Submit a Report" onclick="location.href = 'http://{{.domainname}}/appadmin/report';">
	<br>
	<br>
	<table class="table table-hover table-bordered table-sm">

	    <thead class="blue-grey lighten-4">
	        <tr>
	            <th>Id</th>
	            <th>Name</th>
	            <th>Parent ID</th>
	            <th>Left</th>
	            <th>Right</th>
	        </tr>
	    </thead>
	  
	    <tbody>
	    	{{.Rows}}
	    </tbody>

	</table>
	{{if .ShowNav}}
<br>

<div align="right">
<a href="http://{{.domainname}}/appadmin/index/{{.order}}!0!{{.query}}">&lt;&lt;Start</a>&nbsp;&nbsp;&nbsp;&nbsp;
{{if .showprev}}<a href="http://{{.domainname}}/appadmin/index/{{.order}}!{{.prev}}!{{.query}}">&lt;Prev</a>&nbsp;&nbsp;&nbsp;&nbsp;{{end}}
{{if .next}}<a href="http://{{.domainname}}/appadmin/index/{{.order}}!{{.next}}!{{.query}}">Next&gt;</a>&nbsp;&nbsp;&nbsp;&nbsp;{{end}}
<a href="http://{{.domainname}}/appadmin/index/{{.order}}!{{.end}}!{{.query}}">End&gt;&gt;</a>
</div>
{{end}}
</div>





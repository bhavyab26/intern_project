package routers

import (
	"in_project/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/home", &controllers.MainController{})
    beego.Router("/appadmin/index/:parms", &controllers.AdminController{}, "get,post:Index")
	beego.Router("/appadmin/add/:parms", &controllers.AdminController{}, "get,post:Add")
	beego.Router("/appadmin/report", &controllers.AdminController{}, "get,post:Report")
	
}

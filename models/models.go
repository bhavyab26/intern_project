package models

import (
	"github.com/astaxie/beego/orm"
	"time"
	//"fmt"
)

type SalesOfficer struct {

	Id           int        `form:"id"`
	Name         string     `form:"name" valid:"Required"`
	ParentId     int        `form:"parentid"`
	Email        string     `form:"email" valid:"Email"`
	Lft          int        `form:"lft" valid:"Required"`
	Rgt          int        `form:"rgt" valid:"Required"`
	
	//PhoneNumber  string `valid:"Mobile"`

	// Id int
	// ParentId int `orm:"null; index"`
	// Lft int  `orm:"index"`
	// Rgt int `orm:"index"`
	// Name string
	// Age int16
	// PhoneNumber int `orm:"size(13)"`
	// Address string `orm:"size(60)"`
	// Email string 

	//may or may not use
	////Depth int
	////ChildrenCount int 

}



type Product struct {
	Id int `orm:"column(SKU)"`
	ProductName string `orm:"size(255)"`

}

type Sale struct {
	// Id int `orm:column(Key)`
	// Submitted time.Time `orm:"auto_now_add;type(date)"`
	// SKU *Product `orm:"rel(fk)"`
	// ProdCalls int `orm:"null" valid:"Range(1, 1000)`
	// TotCalls int `orm:"null" valid:"Range(1, 1000)`
	// SalesPerson *SalesOfficer `orm:"rel(fk)"`

	Id int `form:"key"`
	Submitted time.Time `orm:"auto_now_add;type(date)" form:"date"`
	SKU *Product `orm:"rel(fk)" form:"sku"`
	ProdCalls int `form:"prodcalls"valid:"Range(1, 1000)`
	TotCalls int `form:"totcalls" valid:"Range(1, 1000)`
	SalesPerson *SalesOfficer `orm:"rel(fk)"`


}

// func (s *SalesOfficer) TableIndex() [][]string {
//     return [][]string{
//         []string{"ParentId", "lft", "rgt"},
//     }
// }

func init() {
	orm.RegisterModel(new(SalesOfficer), new(Product), new(Sale))
}

// func main() {
// 	o := orm.NewOrm()
// 	user := new(SalesOfficer)
// 	user.Name = "slene"
// 	user.Email = "abc@gmail.com"
// 	user.Lft = 2
// 	user.Rgt = 9


//     // insert
//     id, err := o.Insert(user)
//      fmt.Printf("ID: %d, ERR: %v\n", id, err)
// }